package util

import (
	"log"

	"github.com/spf13/viper"
)


type Config struct{
	DBURL     string `mapstructure:"DBURL"`
}

func LoadConfig()  *Config{
	var config = new(Config)
	viper.SetConfigName(CONFIG_FILE_NAME)
	viper.SetConfigType(CONFIG_FILE_EXTENSION)
	viper.AddConfigPath(APP_ENV_LOCATION)
	viper.AutomaticEnv()
	err:=viper.ReadInConfig()
	if err!=nil{
		log.Fatal(ERROR_STRING_VALUE, err)
	}
	viper.Unmarshal(config)
	return config
}