package data

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

var organisationRepo OrganisationRepository = &OrganisationRepositoryImpl{}

var organisation *Organisation
var organisationOne *Organisation
var savedOrg *Organisation

func organisationTestSetup()  {
	organisation=&Organisation{
		Name: "testOrganisation",
		Address: "testAddress",
	}

	organisationOne=&Organisation{
		Name: "testOrganisationOne",
		Address: "testAddressOne",
	}
}

func TestSave(t *testing.T) {
	organisationTestSetup()
	savedOrg = organisationRepo.Save(organisation)
	savedOrgOne:=organisationRepo.Save(organisationOne)
	log.Println(savedOrg)
	log.Println(savedOrgOne)
	assert.NotNil(t, savedOrg)
	assert.NotNil(t, savedOrgOne)

	assert.NotEmpty(t, savedOrg.Id)
	assert.NotEmpty(t, savedOrgOne.Id)
}

func TestFindById(t *testing.T) {
	organisationTestSetup()
	savedOrg = organisationRepo.Save(organisation)
	assert.NotNil(t, savedOrg)
	assert.NotEmpty(t, savedOrg.Id)

	foundOrg:=organisationRepo.FindById(savedOrg.Id)
	assert.NotNil(t, foundOrg)
	assert.NotEmpty(t, foundOrg.Name)
}

func TestFindAll(t *testing.T) {
	organisations:=organisationRepo.FindAll()
	log.Println(organisations)
	assert.Greater(t, len(organisations), 1)
}

func TestDelete(t *testing.T) {

}

func TestDeleteAll(t *testing.T) {

}