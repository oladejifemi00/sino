package data

import "go.mongodb.org/mongo-driver/bson/primitive"


type Organisation struct{
	Id primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Name string	`bson:"name" json:"name"`
	Address string `bson:"address" json:"address"`
	Email string `bson:"email" json:"email"`
}