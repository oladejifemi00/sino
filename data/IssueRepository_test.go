package data

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

var issueRepository = &IssueRepositoryImpl{}
var issue *Issue
var issue1 *Issue

func issueTestSetUp()  {
	issue=&Issue{
		Name: "test_issue",
	}
	issue1=&Issue{
		Name:"test_Bug",
	}
}

func TestIssueRepositoryImpl_Save(t *testing.T) {
	issueTestSetUp()
	savedIssue:=issueRepository.Save(issue1)
	log.Println(savedIssue)
	assert.NotNil(t,savedIssue)
	assert.NotEmpty(t, savedIssue.Id)
}

func TestIssueRepositoryImpl_FindById(t *testing.T) {
	issueTestSetUp()
	savedIssue:=issueRepository.Save(issue)
	assert.NotNil(t, savedIssue)
	foundIssue:=issueRepository.FindById(savedIssue.Id)
	assert.NotNil(t, foundIssue)
	assert.Equal(t, savedIssue.Name, foundIssue.Name)
}

func TestIssueRepositoryImpl_FindAll(t *testing.T) {
	issueTestSetUp()
	issueRepository.Save(issue)
	issueRepository.Save(issue1)
	log.Println("list-->", issueRepository.FindAll())
	assert.Equal(t, 8, len(issueRepository.FindAll()))
}

func TestIssueRepositoryImpl_Delete(t *testing.T) {
	issueTestSetUp()
	savedIssue:=issueRepository.Save(issue)
	issueRepository.Delete(savedIssue.Id)
	assert.Len(t, issueRepository.FindAll(), 0)
}

func TestIssueRepositoryImpl_DeleteAll(t *testing.T) {
	issueTestSetUp()
	issueRepository.Save(issue)
	issueRepository.Save(issue1)

	issueRepository.DeleteAll()
	assert.Len(t, issueRepository.FindAll(), 0)
}
