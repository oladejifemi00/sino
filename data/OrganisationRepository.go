package data

import (
	"context"
	"log"

	"github.com/djfemz/sino-backend/util"
	"github.com/juju/mgo/v2/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)


type OrganisationRepository interface{
	Save(organisation *Organisation) *Organisation
	FindById(id primitive.ObjectID) *Organisation
	FindAll() []*Organisation
	Delete(id primitive.ObjectID)
	DeleteAll()
}

type OrganisationRepositoryImpl struct{
	
}

func getOrganisationCollection()  *mongo.Collection{
	client:=Connect()
	return client.Database("sino").Collection("organisations")
}

func (organisationRepo *OrganisationRepositoryImpl) Save(organisation *Organisation) *Organisation  {
	orgCollection:= getOrganisationCollection()
	ctx:=context.TODO()
	result, err:=orgCollection.InsertOne(ctx,organisation)
	if err!=nil{
		log.Fatal(err)
	}
	foundOrg:=&Organisation{}
	foundResult:=orgCollection.FindOne(ctx, bson.M{"_id":result.InsertedID})
	err=foundResult.Decode(foundOrg)
	if err!=nil{
		log.Fatal(err)
	}
	return foundOrg
}

func (organisationRepo *OrganisationRepositoryImpl) FindById(id primitive.ObjectID) *Organisation{
	foundOrg:=&Organisation{}
	organisationCollection:= getOrganisationCollection()
	result:=organisationCollection.FindOne(context.TODO(), bson.M{"_id":id})
	err:=result.Decode(foundOrg)
	if err!=nil{
		log.Fatal(err)
	}
	return foundOrg
}

func (organisationRepo *OrganisationRepositoryImpl) FindAll() []*Organisation{
	foundOrgs:=[]*Organisation{}
	ctx:=context.TODO()
	collection:=getOrganisationCollection()
	cur, err:=collection.Find(ctx, bson.M{})
	if err!=nil{
		log.Fatal(err)
	}
	for cur.Next(ctx){
		org:=&Organisation{}
		err=cur.Decode(org)
		if err!=nil{
			log.Fatal(util.ERROR_STRING_VALUE,err)
		}
		foundOrgs = append(foundOrgs, org)
	}
	return foundOrgs
}

func (organisationRepo *OrganisationRepositoryImpl) Delete(id primitive.ObjectID){
	
}

func (organisationRepo *OrganisationRepositoryImpl) DeleteAll(){
	
}