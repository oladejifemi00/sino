package data

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Issue struct{
	Id primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Name string	`bson:"name" json:"name"` 
}