package data

import (
	"context"
	"log"
	"time"

	"github.com/djfemz/sino-backend/util"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Dev struct{
	Id primitive.ObjectID `bson:"_id,omitempty"`
	Name string `bson:"name"`
	Email string `bson:"email"`
	Role Role `bson:"role"`
}

type Role struct{
	Id primitive.ObjectID `bson:"_id,omitempty"`
	Name string `bson:"name"`
}

func Connect() *mongo.Client {
	DB_URL := util.LoadConfig().DBURL

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(DB_URL))
	if err!=nil{
		log.Fatal("error: ", err)
	}
	return client
}