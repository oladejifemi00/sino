package data

import (
	"context"
	"log"

	"github.com/juju/mgo/v2/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var client = Connect()
type DevRepository interface{
	Save(dev *Dev) *Dev
	FindById(id primitive.ObjectID) *Dev
	FindAll() []*Dev
	Delete(id primitive.ObjectID)
	DeleteAll()
}

type DevRepositoryImpl struct{}

func getDevsCollection() *mongo.Collection{
	mgo:= mongo.IndexModel{
		Keys: bson.M{
			"email":1,
		},
		Options: options.Index().SetName("email").SetUnique(true),
	}
	col:=client.Database("sino").Collection("devs")
	
	name, err:=col.Indexes().CreateOne(context.TODO(), mgo)
	if err!=nil {
		log.Fatal("error when creating unique field: ", err)
	}
	log.Println(name)
	return col
}


func (devRepo *DevRepositoryImpl) Save(dev *Dev) *Dev {
	var foundDev = &Dev{}
	ctx:=context.TODO()
	collection:=getDevsCollection()
	savedDev, err:= collection.InsertOne(ctx, dev)
	if err!=nil{
		log.Fatal("error: ", err)
	}
	err=collection.FindOne(ctx, bson.M{"_id": savedDev.InsertedID}).Decode(foundDev)
	if err!=nil{
		log.Fatal("error finding one: ", err)
	}
	log.Println("result-->", foundDev)

	return foundDev
}

func (devRepo *DevRepositoryImpl) FindById(id primitive.ObjectID) *Dev{
	foundDev:=&Dev{}
	collection:=getDevsCollection()
	err:=collection.FindOne(context.TODO(), bson.M{"_id":id}).Decode(foundDev)
	if err!=nil{
		log.Fatal("error finding one: ", err)
	}
	log.Println("found guy-->", foundDev)
	return foundDev
}

func (devRepo *DevRepositoryImpl) FindAll() []*Dev{
	devs:= []*Dev{}
	collection:=getDevsCollection()
	cur, err:= collection.Find(context.TODO(), bson.M{}, options.Find())
	if err!=nil{
		log.Fatal("error: ", err)
	}
	for cur.Next(context.TODO()){
		var dev =&Dev{}
		err=cur.Decode(dev)
		if err!=nil{
			log.Fatal("error: ", err)
		}
		devs = append(devs, dev)
	}
	return devs
}

func (devRepo *DevRepositoryImpl) Delete(id primitive.ObjectID){
	collection:=getDevsCollection()
	_,err:=collection.DeleteOne(context.TODO(), bson.M{"_id":id})
	if err!=nil{
		log.Fatal("error deleting one: ",err)
	}
}

func (devRepo *DevRepositoryImpl) DeleteAll(){
	collection:=getDevsCollection()
	_, err:=collection.DeleteMany(context.TODO(), bson.M{})
	if err!=nil{
		log.Fatal("error deleting all: ", err)
	}
}