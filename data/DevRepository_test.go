package data

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

var devRepo DevRepository = &DevRepositoryImpl{}
var dev *Dev
var dev1 *Dev
var dev3 *Dev
func setUp()  {
	dev=&Dev{
		Name: "John91",
		Role: Role{
			Name: "BACKEND_ENG",
		},
		Email: "test@gmail.com",
	}
	dev1=&Dev{
		Name:"Jane",
		Role: Role{
			Name: "FRONTEND_ENG",
		},
		Email: "test1@gmail.com",
	}
	dev3=&Dev{
		Name: "Jon91",
		Role: Role{
			Name: "BACKEND_ENG",
		},
		Email: "testy@gmail.com",
	}
}

func tearDown(){
	
}


func TestDevRepositoryImpl_Save(t *testing.T) {
	setUp()
	savedDev := devRepo.Save(dev)
	log.Println("saved dev-->", savedDev)
	
	assert.NotNil(t, savedDev)
}

func TestDevRepositoryImpl_FindById(t *testing.T) {
	setUp()
	savedDev2:=devRepo.Save(dev1)
	log.Println(savedDev2.Id)
	foundDev:=devRepo.FindById(savedDev2.Id)
	log.Println("found in find by id-->", foundDev)
	assert.NotNil(t, foundDev)
	assert.Equal(t, savedDev2.Name, foundDev.Name)
}

func TestDevRepositoryImpl_FindAll(t *testing.T) {
	setUp()
	devRepo.Save(dev)
	devRepo.Save(dev1)
	devs:=devRepo.FindAll()
	assert.Equal(t, 2, len(devs))
}

func TestDevRepositoryImpl_Delete(t *testing.T) {
	setUp()
	savedDev:=devRepo.Save(dev)
	size:=len(devRepo.FindAll())
	devRepo.Delete(savedDev.Id)
	assert.Equal(t, size-1, len(devRepo.FindAll()))
}

func TestDevRepositoryImplDeleteAll(t *testing.T) {
	devRepo.DeleteAll()
	assert.Equal(t, 0, len(devRepo.FindAll()))
}

func TestDevRepositoryCannotTakeMultipleDocsWithSameEmail(t *testing.T) {
	setUp()
	devRepo.Save(dev)
	devRepo.Save(dev1)
	devRepo.Save(dev3)
	assert.Equal(t, 3, len(devRepo.FindAll()))
}
