package data

import (
	"context"
	"log"

	"github.com/juju/mgo/v2/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type IssueRepository interface{
	Save(issue *Issue) *Issue
	FindById(id primitive.ObjectID) *Issue
	FindAll() []*Issue
	Delete(id primitive.ObjectID)
	DeleteAll()
}

type IssueRepositoryImpl struct{
	
}



func getIssuesCollection()  *mongo.Collection{
	return client.Database("sino").Collection("issues")
}

func (issueRepo *IssueRepositoryImpl) Save(issue *Issue) *Issue  {
	collection:=getIssuesCollection()
	res, err:=collection.InsertOne(context.TODO(), issue)
	if err!=nil{
		log.Fatal("error saving issue:", err)
	}
	var foundIssue =&Issue{}
	collection.FindOne(context.TODO(), bson.M{"_id":res.InsertedID}).
	Decode(foundIssue)
	return foundIssue
}

func (issueRepo *IssueRepositoryImpl) FindById(id primitive.ObjectID) *Issue{
	foundIssue:=&Issue{}
	collection:=getIssuesCollection()
	collection.FindOne(context.TODO(), bson.M{"_id":id}).
	Decode(foundIssue)
	return foundIssue
}

func (issueRepo *IssueRepositoryImpl) FindAll() []*Issue{
	var issueList=[]*Issue{}
	collection:=getIssuesCollection()
	cur, err:=collection.Find(context.TODO(), bson.M{})
	if err!=nil{
		log.Fatal("error finding all issues: ", err)
	}
	for cur.Next(context.TODO()){
		iss:=&Issue{}
		err=cur.Decode(iss)
		if err!=nil{
			log.Fatal("error decoding issue: ", err)
		}
		issueList=append(issueList, iss)
	}
	return issueList
}

func (issueRepo *IssueRepositoryImpl) Delete(id primitive.ObjectID){
	collection:=getIssuesCollection()
	_, err:=collection.DeleteOne(context.TODO(), bson.M{"_id":id})
	if err!=nil{
		log.Fatal("error deleting issue: ", err)
	}
}

func (issueRepo *IssueRepositoryImpl) DeleteAll(){
	collection:=getIssuesCollection()
	_, err:=collection.DeleteMany(context.TODO(), bson.M{})
	if err!=nil{
		log.Fatal(err)
	}
}